module.exports = function(){
  var _ = require('lodash')
  var faker = require('faker')

  return {
    randomPeople: _.times(150, function(n){
      return {
        id: n,
        name: faker.name.findName(),
        avatar: faker.internet.avatar()
      }
    })
  }
}