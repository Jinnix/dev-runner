import './css/app.styl';

import React from 'react';
import { Router, Route, Link, hashHistory } from 'react-router';
import PageTitle from './pageTitle';

const Home = ()=> 
  <div>
    <PageTitle 
        imgs={[<img src={'./img/logo-redux.png'} width="150" />, <img src={'./img/react.png'} width="170" />]}
        title='{ simple dev runner }'
    />
    <h2>Greetings...</h2>
  </div>

class App extends React.Component {
  render() {
    return(
      <Router history={ hashHistory }>
        <Route path="/" component={Home}></Route>
      </Router>
    )
  }
}

export default App;