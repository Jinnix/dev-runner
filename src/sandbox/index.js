import React from 'react';
import PageTitle from '../pageTitle.js';
import Links from '../navLinks.js';

class Sandbox extends React.Component {
  render(){
    return(
      <div>
        <PageTitle title='{ Case study 2: "Handling async with promise" }' />
        <Links />
        <div className="text-center"><img src='./sandbox/mindblow.gif' width="50%"/></div>
      </div>
    )
  }
}

export default Sandbox;