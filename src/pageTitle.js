import React from 'react';

let index=0
const PageTitle = ({imgs, title}) =>
  <h1 className="pageTitle">
    {imgs ? imgs.map(img=><span key={index++}>{img}</span>) : null}
    <span>{title}</span>
  </h1>

export default PageTitle;