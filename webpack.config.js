var webpack = require('webpack');
var yeticss = require('yeticss');

module.exports = {
  // Entry point for the bundle, path and filename to main module
  entry: './src/index.js',
  output: {
    // Output directory as an absolute path
    path: './src',
    // Filename as relative path within output path
    filename: 'bundle.js',
    // Output path from the view of the JS/HTML
    publicPath: '/'
  },
  devServer: {
    inline: true,
    contentBase: './src',
    port: 3333
  },
  module: {
    // Loader required for JSX
    loaders: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: 'babel',
        query:{
          plugins: ['transform-decorators-legacy']
        }
      },
      {
        test: /\.css$/,
        loader: "style-loader!css-loader"
      },
      {
        test: /\.woff$/,
        loader: 'url?limit=65000&mimetype=application/font-woff&name=src/css/[name].[ext]'
      },
      { 
        test: /\.styl$/, loader: 'style-loader!css-loader!stylus-loader'
      }
    ]
  },

  stylus: {
    use: [yeticss()]
  }
};